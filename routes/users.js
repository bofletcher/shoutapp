var express = require("express");
var router = express.Router();
var cassandra = require("cassandra-driver");

var client = new cassandra.Client({ contactPoints: ["127.0.0.1"] });
client.connect(function (err, result) {
  console.log("cassandra connected");
});

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

module.exports = router;
